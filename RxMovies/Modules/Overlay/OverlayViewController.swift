//
//  OverlayViewController.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 22.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import UIKit
import Kingfisher



class OverlayViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var movieImageView: UIImageView!
    
    var movieItem: NowPlaying!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var previewActionItems: [UIPreviewActionItem] {
        
        let action1 = UIPreviewAction(title: "Zu Favoriten hinzufügen", style: .destructive) { (action, vc) in
            print("added to fav")
        }
        
        return [action1]
    }
    
    func configureView() {
        if let movie = self.movieItem {
            if !self.movieItem.description.isEmpty {
                self.descriptionTextView.text = movie.description
            } else {
                self.descriptionTextView.text = "Leider keine Beschreibung vorhanden : ("
            }
            let imageURL: URL = URL(string: NowPlaying.imageBaseURL + "/" + movie.imagePath)!
            
            self.navBar.topItem?.title = movie.name
            self.movieImageView.kf.setImage(with: imageURL, placeholder: nil, options: [.transition(.fade(1))], progressBlock: nil, completionHandler: nil)
            
            
        }
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
            self.configureView()
        
        
        
        //descriptionTextView.textAlignment = .center
        
        movieImageView.layer.shadowRadius = 2
        movieImageView.layer.shadowColor = UIColor.white.cgColor
        movieImageView.layer.shadowOffset = CGSize.zero
        movieImageView.layer.shadowOpacity = 1
        //print(movieItem.description!)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Utils.makeDark(self.view)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
   
    
}
