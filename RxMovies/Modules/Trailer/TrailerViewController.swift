//
//  TrailerViewController.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 23.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TrailerViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var items:Variable<[String]> = Variable<[String]>(["Hallo", "Hallo", "Hallo", "Hallo","Hallo", "Hallo","Hallo", "Hallo","Hallo", "Hallo"])
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationItem.largeTitleDisplayMode = .never
            let search = UISearchController(searchResultsController: nil)
            search.searchResultsUpdater = self
            search.searchBar.scopeButtonTitles = ["Filme", "Serien", "Schauspieler"]
            self.navigationItem.searchController = search
            self.navigationController?.navigationBar.largeTitleTextAttributes = [NSForegroundColorAttributeName: UIColor.red]
            self.navigationItem.hidesSearchBarWhenScrolling = false
        }

        tableView.tableFooterView = UIView()
     
        items
            .asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: "SearchCell")) { row, element, cell in
                cell.textLabel?.text = element
            }.disposed(by:disposeBag)
        
        
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension TrailerViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
    }
}
