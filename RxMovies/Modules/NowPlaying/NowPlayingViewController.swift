//
//  NowPlayingViewController.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 22.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class NowPlayingViewController: UITableViewController, UIViewControllerPreviewingDelegate {

    var nowPlaying: Variable<[NowPlaying]> = Variable<[NowPlaying]>([])
    var selectedMovieByPeak: NowPlaying?
    
    let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NowPlaying.getNowPlaying()
        
        tableView.dataSource = nil
        
        // check if force touch is available
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: tableView!)
        }
        
//        NowPlaying.nowPlaying
//            .asObservable()
//            .bind(to: self.nowPlaying)
//            .disposed(by: disposeBag)
        
        nowPlaying
            .asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: NowPlayingCell.self)) { index, element, cell in
                
                let imageURL = URL(string: NowPlaying.imageBaseURL + "/" + element.imagePath)
                
                cell.movieTitleLabel.text = element.name
                cell.movieImageView.kf.setImage(with: imageURL, placeholder: nil, options: [.transition(.fade(0.5))], progressBlock: nil, completionHandler: nil)
                
                
        }.disposed(by: disposeBag)
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        tableView.rx.itemSelected
            .subscribe(onNext: { indexPath in
                 vc.movieObj = self.nowPlaying.value[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }).disposed(by: disposeBag)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
    }
    
  // PEEK
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        selectedMovieByPeak = nil
        guard let indexPath = tableView.indexPathForRow(at: location),
            let cell = tableView.cellForRow(at: indexPath) else { return nil }
        
        selectedMovieByPeak = nowPlaying.value[indexPath.row]
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let overlayVC = sb.instantiateViewController(withIdentifier: "Overlay") as! OverlayViewController
        overlayVC.movieItem = selectedMovieByPeak
        
        previewingContext.sourceRect = cell.frame
        overlayVC.preferredContentSize = CGSize(width: 0, height: 500)

        
        return overlayVC
    }
    
    // POP
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let movieDetailVC = sb.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        if let availableMovie = selectedMovieByPeak {
            movieDetailVC.movieObj = availableMovie
            show(movieDetailVC, sender: self)
        }
    }
}
