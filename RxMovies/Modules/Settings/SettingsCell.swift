//
//  SettingsCell.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 24.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

    
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var cacheMBLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = UIColor.black
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
