//
//  DetailViewController.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 22.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {

    @IBOutlet weak var movieImageViwe: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieDesc: UITextView!
    
    var movieObj: NowPlayingType?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let url = URL(string: NowPlaying.imageBaseURL + "/" + (movieObj?.imagePath)!)
        movieImageViwe.kf.setImage(with: url!)
        self.movieDesc.text = movieObj?.description

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.movieImageViwe.layer.shadowColor = UIColor.white.cgColor
        self.movieImageViwe.layer.shadowOffset = CGSize.zero
        self.movieImageViwe.layer.shadowRadius = 3
        self.movieImageViwe.layer.shadowOpacity = 1
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.title = movieObj?.name
        
        print("hello")
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.tintColor = UIColor.white

    }
    
    deinit {
        let t = true
    }
}
