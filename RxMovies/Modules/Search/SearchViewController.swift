//
//  SearchViewController.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 25.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher
import RxDataSources

class SearchViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    var actors: Variable<[Actor]> = Variable<[Actor]>([])
    
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.rx.value.orEmpty
            .asObservable()
            .filter({ $0.isEmpty })
            .subscribe(onNext: { [weak self] _ in
                Actor.actor.value.removeAll()
                self?.actors.value.removeAll()
            }).disposed(by: disposeBag)
        
        
        // toDo: implement shit
//            segmentedControl.rx.value
//            .asObservable()
//            .subscribe(onNext: { _ in
//                switch self.segmentedControl.selectedSegmentIndex {
//                case 0: break // toDo: implement
//                case 1: break// toDo: implement
//                case 2: self.searchActor()
//                default: break
//                }
//            }, onCompleted: {
//
//            }).disposed(by: disposeBag)

        actors
            .asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: "SearchCell", cellType: SearchCell.self)) { row, element, cell in
                cell.nameLabel?.text = element.name
                cell.knownForLabel?.text = "\(element.knownFor[0])"
                cell.actorImage?.kf.setImage(with: URL(string: Actor.imageBaseURL + "/" + element.profileImagePath), placeholder: nil, options: [.transition(.fade(0.5))], progressBlock: nil, completionHandler: nil)

            }.disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

    func searchActor() {
        searchBar.rx.text.orEmpty
            .asObservable()
            .debounce(1, scheduler: MainScheduler.instance)
            .filter({ !$0.isEmpty })
            .subscribe(onNext: { query in
                self.actors.value.removeAll()
                Actor.getActor(query, self.view)
        }).disposed(by: disposeBag)
    }
}
