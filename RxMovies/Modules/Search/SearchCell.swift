//
//  SearchCell.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 25.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

    
    @IBOutlet weak var actorImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var knownForLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.actorImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        actorImage.clipsToBounds = true
    }

}
