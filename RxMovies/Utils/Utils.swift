//
//  Utils.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 24.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    
    public static var defaultButtons : [String] = ["Like"]
    public static var defaultButtonColor : UIColor = .white
    public static var cancelButtonColor  : UIColor = .white
    public static var seperatorColor     : UIColor = .white
    
    public class func makeDark(_ selfView: UIView) {
        if let uiwindow : UIWindow = selfView.window {
            for temp in uiwindow.subviews {
                if temp.description.contains("UITransitionView") {
                    // color effect view
                    let array1 : [UIVisualEffectView] = Utils.getSubviewsOf(view: temp)
                    
                    for v in array1 {
                        let blur = UIBlurEffect(style: .dark)
                        v.effect = blur
                    }
                    
                    // color buttons
                    let array2 : [UIStackView] = Utils.getSubviewsOf(view: temp)
                    
                    for v in array2 {
                        for temp in v.subviews {
                            var first = true
                            for t in temp.subviews {
                                if first {
                                    first = false
                                    for sub in t.subviews {
                                        if let label = sub as? UILabel {
                                            if Utils.defaultButtons.contains(label.text!) {
                                                label.textColor = Utils.defaultButtonColor
                                                label.tintColor = Utils.defaultButtonColor
                                            }else {
                                                label.textColor = Utils.cancelButtonColor
                                                label.tintColor = Utils.cancelButtonColor
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    
                    let array3 : [UIView] = Utils.getSubviewsOf(view: temp)
                    
                    for lol in array3 {
                        if lol.description.contains("UIInterfaceActionItemSeparatorView") {
                            for damn in lol.subviews {
                                damn.backgroundColor = Utils.seperatorColor
                            }
                        }
                    }
                    
                }
            }
        }
    }
    
    private class func getSubviewsOf<T : UIView>(view:UIView) -> [T] {
        var subviews = [T]()
        
        for subview in view.subviews {
            subviews += getSubviewsOf(view: subview) as [T]
            
            if let subview = subview as? T {
                subviews.append(subview)
            }
        }
        
        return subviews
    }
    
}
