//
//  TopRated.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 24.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import SwiftyJSON

public struct TopRated {

    static let APIKEY = "4c0b8d236e387a7c24bc97ca0b0ecf03"
    static let baseUrl = "https://api.themoviedb.org/3/movie/top_rated?api_key=\(APIKEY)&language=de_DE"
    static let imageBaseURL = "https://image.tmdb.org/t/p/w342"
    static let disposeBag = DisposeBag()

    var name: String!
    var description: String!
    var imagePath: String!
    var voteAverage: Double!

    public static var topRated: Variable<[TopRated]> = Variable<[TopRated]>([])

    public init(name: String, description: String, imagePath: String, voteAverage: Double) {
        self.name = name
        self.description = description
        self.imagePath = imagePath
    }

    public static func getTopRatedMovies() {

        RxAlamofire.request(.get, URL(string: baseUrl)!)
            .debug()
            .flatMap({ request in
                return request.validate(statusCode: 200..<300)
                    .validate(contentType: ["application/json"])
                    .rx.json()
            }).observeOn(MainScheduler.instance)
            .subscribe(onNext: { data in

                let json = JSON(data)
                let topRatedResults = json["results"][].array
                
                for movie in topRatedResults! {
                    let title = movie["title"].stringValue
                    let description = movie["description"].stringValue
                    let imagePath = movie["poster_path"].stringValue
                    let voteAverage = movie["vote_average"].doubleValue
                    
                    let movieObj = TopRated(name: title, description: description, imagePath: imagePath, voteAverage: voteAverage)
                    
                    topRated.value.append(movieObj)
                    
                }
            }, onError: { error in
                print(error.localizedDescription)
            }).disposed(by: disposeBag)
    }
}
