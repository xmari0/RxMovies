//
//  Movie.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 22.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire
import SwiftyJSON


public protocol NowPlayingType {
    var name: String { get set }
    var description: String { get set }
    var imagePath: String { get set }
}

public struct NowPlayingSearch {

    /// Methode um API Fehler zu beschreiben & darzustellen.
    ///
    /// - Parameter error:
    /// - Returns: Custom Error
    public static func apiError(_ error: String) -> NSError {
        return NSError(domain: "NowPlayingSearch", code: -1, userInfo: [NSLocalizedDescriptionKey: error])
    }

    /// Nimmt die JSON entgegen und zieht sich die gesuchten Werte heraus.
    ///
    /// - Parameter json: Typ JSON (SwiftyJSON)
    /// - Returns: Observable Array of type NowPlaying
    /// - Throws: apiError
    public static func parseJSON(_ json: JSON) throws -> Observable<[NowPlaying]> {

        let nowPlaying: Variable<[NowPlaying]> = Variable<[NowPlaying]>([])

        let json = JSON(json)
        guard let results = json["results"][].array else {
            throw apiError("error parsing json")
        }
        for movies in results {

            guard let title = movies["title"].string else {
                throw apiError("Kein Titel gefunden.")
            }
            guard let description = movies["overview"].string else {
                throw apiError("Keine Beschreibung gefunden : (")
            }
            guard let imagePath = movies["poster_path"].string else {
                throw apiError("Kein Poster gefunden.")
            }

            let movieObj = NowPlaying(name: title, description: description, imagePath: imagePath)
            nowPlaying.value.append(movieObj)
        }
        return nowPlaying.asObservable()
    }
}

/// Eine Klasse um die aktuellen Filme welche im Kino gespielt werden zu holen.
public class NowPlaying: NowPlayingType {

    static let APIKEY = "4c0b8d236e387a7c24bc97ca0b0ecf03"
    static let baseUrl = "https://api.themoviedb.org/3/movie/now_playing?api_key=\(APIKEY)&language=de-DE&append_to_response=translations"
    static let imageBaseURL = "https://image.tmdb.org/t/p/w342"
    static let disposeBag = DisposeBag()

    public var name: String
    public var description: String
    public var imagePath: String

    public init(name: String, description: String, imagePath: String) {
        self.name = name
        self.description = description
        self.imagePath = imagePath
    }

    /// Input URL zu JSON
    ///
    /// - Parameter url: Egaaal was hauptsache die API liefert eine JSON zurück :D
    /// - Returns: Observable<Any>
    private static func JSONN(_ url: URL) -> Observable<Any> {
        return URLSession.shared.rx
            .json(url: url)
            .retry(3)
    }

    /// Ziel ist es, aus einer URL eine JSON zu formen. Der Result der **JSONN** Methode kommt als Any zurück, wesswegen wir den Result mappen müssen zu einer tatsächlichen JSON. Diese versuchen wir dann zu parsen.
    ///
    /// - Returns: Observable Array of Type NowPlaying
    public static func getNowPlayingResults() -> Observable<[NowPlaying]> {
        return JSONN(URL(string: baseUrl)!)
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .map({ jsonResult in
                guard let json = jsonResult as? JSON else {
                    throw NowPlayingSearch.apiError("failed to parse data")
                }
                // ?? ?? ?? ?? ?? ?? ? ??? ? ? ?? ??? __?? _?_?_???DA?S?DKFJDFGHGT?$%IT§$%)§§$RGKFJG§)$%§$JTFD
                return try NowPlayingSearch.parseJSON(json)
                // ARE U KIDDING MEEEEEEEEEEEEEEEE?
                // guess yes
                // -> Observable<[Nowplaying]>
                // lolololo

            })
            .observeOn(MainScheduler.instance)
    }
}
