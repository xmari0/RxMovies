//
//  Trailer.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 23.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire


public struct SearchActors {
    
    static let APIKEY = "4c0b8d236e387a7c24bc97ca0b0ecf03"
    static let baseUrl = "https://api.themoviedb.org/3/search/movie?api_key=\(APIKEY)&language=de-DE&query="
    static let imageBaseURL = "https://image.tmdb.org/t/p/w342"
    static let disposeBag = DisposeBag()
    
    var name: String!
    var profileImage: UIImage!
    
    public init (name: String, profileImage: UIImage) {
        self.name = name
        self.profileImage = profileImage
    }
    
    public static func getActor(byName name:String) {
     
}
}
