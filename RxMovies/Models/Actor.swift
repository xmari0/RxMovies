//
//  Actor.swift
//  RxMovies
//
//  Created by Mario Zimmermann on 25.08.17.
//  Copyright © 2017 Mario Zimmermann. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire
import SwiftyJSON

protocol ActorsType {
    
    var name : String { get set }
    var profileImagePath : String { get set }
    var knownFor : [String] { get set }
    var popularity: Double { get set }
    
}

public struct Actor: ActorsType {
    
    var name                : String
    var knownFor            : [String]
    var profileImagePath    : String
    var popularity          : Double
    
    static let APIKEY           : String = "4c0b8d236e387a7c24bc97ca0b0ecf03"
    static let baseUrl          : String = "https://api.themoviedb.org/3/search/person?api_key=\(APIKEY)&language=de-DE&query="
    static let imageBaseURL     : String = "https://image.tmdb.org/t/p/w342"
    static let disposeBag = DisposeBag()
    
    public init(name : String, knownFor : [String], profileImagePath : String, popularity: Double) {
        self.name = name
        self.knownFor = knownFor
        self.profileImagePath = profileImagePath
        self.popularity = popularity
    }
    
    public static var actor: Variable<[Actor]> = Variable<[Actor]>([])

    
    public static func getActor(_ name: String,_ view: UIView) {
        
        RxAlamofire.request(.get, URL(string: baseUrl + name)!)
            .debug()
            .flatMap({ request in
                return request.validate(statusCode: 200..<300)
                    .validate(contentType: ["application/json"])
                    .rx.json()
            }).observeOn(MainScheduler.instance)
            .subscribe(onNext: { data in
                let json = JSON(data)
                print(json)
                let actorResults = json["results"][].array
                
                for actor in actorResults! {
                    
                    var knownForArray = [String]()
                    
                    let name = actor["name"].stringValue
                    let imagePath = actor["profile_path"].string
                    let popularity = actor["popularity"].doubleValue
                    let knownFor = actor["known_for"].array
                    for movies in knownFor! {
                        knownForArray.append(movies["title"].stringValue)
                    }
                    
                    guard let iPath = imagePath else { continue }
                    
                    let actorObj = Actor(name: name, knownFor: knownForArray, profileImagePath: iPath, popularity: popularity)
                    
                    self.actor.value.append(actorObj)
                    
                }
                
                
            }, onCompleted: {
                view.endEditing(true)
            }).disposed(by: disposeBag)
    }
}
